<?php

class TopUrlLevelGetter {
  private $sSiteURL = ''; // should always be something like: http://nysna.docksal
  private $aPageQueue = [];
  private $aProcessedPageList = [];
  private $iMaxLevel = 1;
  private $aSkipRegexes = [];

  function addSkipRegex($sRegex) {
    array_push($this->aSkipRegexes, $sRegex);
  }

  function saveURLListForTopXLevels($sSiteURL, $iLevels, $sFilePath) {
    $aProcessedPageList = $this->getURLListForTopXLevels($sSiteURL, $iLevels);
    $sFileContents = implode("\n", array_keys($aProcessedPageList));

    file_put_contents($sFilePath, $sFileContents);
  }

  function getProcessedCount() {
    return(sizeof($this->aProcessedPageList));
  }

  function printURLListForTopXLevels($sSiteURL, $iLevels) {
    $aProcessedPageList = $this->getURLListForTopXLevels($sSiteURL, $iLevels);
    print implode("\n", array_keys($aProcessedPageList));
    print "\n";
  }

	function getURLListForTopXLevels($sSiteURL, $iLevels) {

	  // get site info for spidering
    $aUrlParts = parse_url($sSiteURL);
    $this->sSiteURL = $aUrlParts['scheme']."://".$aUrlParts['host'];

    $this->addPageToQueue($sSiteURL,  0);
    $this->iMaxLevel = $iLevels;

    $this->processQueue();

    return($this->aProcessedPageList);
	}

	function processQueue() {

	  $iRunawayCounter = 0;
    while (($aPageRecord = $this->getNextUnprocessedPageRecord()) != null && $iRunawayCounter++ < 10000) {

      if ($iRunawayCounter % 20 == 0 || $iRunawayCounter == 1) {
        print "Count: $iRunawayCounter\n";
      }
      $iPageLevel = $aPageRecord['level'];
      // max level already? just shift to processed pages
      if ($iPageLevel > $this->iMaxLevel) {
        $this->addPageRecordToProcessedQueue($aPageRecord);
        continue;
      }

//      print "Looking at ".$aPageRecord['url']."\n";
      // can look into it
      $aSubpageList = $this->getPageSubitemsWithHostname($aPageRecord['url']);
      foreach ($aSubpageList as $sPageURL) {
        if (!$this->hasPageBeenSeenAlready($sPageURL)) {
          if (!$this->shouldSkipPath($sPageURL)) {
            $this->addPageToQueue($sPageURL, $iPageLevel+1);
          }
        }
      }
    }
  }

  function shouldSkipPath($sURL) {
    foreach ($this->aSkipRegexes as $sSkipRegex) {
      if (preg_match($sSkipRegex, $sURL)) {
        return(true);
      }
    }
    return(false);
  }

  function getPageSubitemsWithHostname($sURL) {
	  $aPages = $this->getPageSubitems($sURL);

	  $aFullPages = [];
	  foreach ($aPages as $sPageRelativeURL) {
	    array_push($aFullPages, $this->sSiteURL.$sPageRelativeURL);
    }

	  return($aFullPages);
  }
	/**
   * Look for local href links only
	 */
	function getPageSubitems($sURL) {

	  /* Raw command will look like this
      wget -q http://nysna.docksal -O - | \
        tr "\t\r\n'" '   "' | \
        grep -i -o '<a[^>]\+href[ ]*=[ \t]*"\/[^"]\+"' | \
        sed -e 's/^.*"\([^"]\+\)".*$/\1/g'
    */
	  $sCommand = "wget -q $sURL -O -";
	  $aPipes = [
      'tr "\t\r\n'."'".'" '."'".'   "'."'",
      'grep -i -o '."'".'<a[^>]\+href[ ]*=[ \t]*"\/[^"]\+"'."'",
      'sed -e '."'".'s/^.*"\([^"]\+\)".*$/\1/g'."'"
    ];

    $sProperCommand = $sCommand." | ".implode(' | ', $aPipes);
    exec($sProperCommand, $aURLList, $iReturn);

    return($aURLList);
  }

  function addPageToQueue($sURL, $iLevel) {
    $aPageRecord = [
      'url' => $sURL,
      'level' => $iLevel
    ];
    $this->aPageQueue[$sURL] = $aPageRecord;
  }

  function getNextUnprocessedPageRecord() {
	  if (sizeof($this->aPageQueue) == 0) {
	    return(null);
    }
    $aPageRecord = array_shift($this->aPageQueue);
    return($aPageRecord);
  }

  function hasPageBeenSeenAlready($sURL) {
    $bIsInQueue = array_key_exists($sURL, $this->aPageQueue);
    $bHasBeenProcessed = array_key_exists($sURL, $this->aProcessedPageList);

    return($bIsInQueue || $bHasBeenProcessed);
  }

  function addPageRecordToProcessedQueue($aPageRecord) {
	  $sURL = $aPageRecord['url'];
    $this->aProcessedPageList[$sURL] = $aPageRecord;
  }
}
