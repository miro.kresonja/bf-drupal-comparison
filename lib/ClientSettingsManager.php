<?php

include_once(dirname(__FILE__)."/"."SitePuller.php");
include_once(dirname(__FILE__)."/"."TopLevelUrlGetter.php");
include_once(dirname(__FILE__)."/"."CommandRunner.php");
include_once(dirname(__FILE__)."/"."ComparisonManager.php");

class ClientSettingsManager {
  public $sWorkingDir = '';
  public $sClientName = '';
  public $sSiteURL = '';
  public $aExclusionDirs = '';

  public $aArgs = [];

  function __construct() {

    $this->sWorkingDir = dirname(dirname(__FILE__)); // can be overridden?

    $this->aArgs = CommandRunner::getArguments();
    $this->readSettings();
  }

  function readSettings() {
    $sSettingsFile = $this->sWorkingDir."/clientsettings.php";
    if (!file_exists($sSettingsFile)) {
      die("Settings file not available ($sSettingsFile).");
    }
    include_once($sSettingsFile);
    $this->sSiteURL = $sSiteURL;
    $this->aExclusionDirs = $aExclusionDirs;
  }

  function getSiteListFilePath() {
    $sFilePath = dirname(__FILE__)."/"."../url.list";
    return($sFilePath);
  }
}