<?php

class SitePuller {
  /**
   * @var ClientSettingsManager $oClientSettings
   */
  private $oClientSettings;
  private $sRunName;
  private $sRunDir;

  function __construct($oClientSettings) {
    $this->oClientSettings = $oClientSettings;
  }

  function pullSite($sRunName = '') {
    if ($sRunName == '') {
      $this->sRunName = date("YmdHis");
    }
    $this->sRunName = $sRunName;
    $this->establishRunDir($sRunName);
    $this->runPull();
  }

  function establishRunDir($sRunName) {
    $this->sRunDir = self::getRunDir($sRunName);
    @mkdir($this->sRunDir, 0777, true);

    if (!file_exists($this->sRunDir)) {
      die("Cannot establish run directory ".$this->sRunDir."\n");
    }
  }

  function runPull() {

    $siteListFilePath = $this->oClientSettings->getSiteListFilePath();
    $sListOfPaths = file_get_contents($siteListFilePath);
    $aListOfPaths = explode("\n", $sListOfPaths);

    chdir($this->sRunDir);
    foreach ($aListOfPaths as $sPath) {
        $sPath = $this->prepPath($sPath);
        if ($sPath != '') {
            $wgetCommand = "wget -nv $sPath";
            system ($wgetCommand);
        }
    }
//    system ('pwd');
  }

  function prepPath($sPath) {
    if (substr($sPath, 0, 4) == "http") {
        return($sPath);
    }
    // is it a comment? skip
    if (substr($sPath, 0, 1) == '#') {
	return('');
    } 
    // otherwise, get the site name from client settings, prepend a path
    if (substr($sPath, 0, 1) == '/') {
	return($this->oClientSettings->sSiteURL.$sPath);
    } 
    return('');
  }
  static function getRunDir($sRunName) {
    $sRunDir = dirname(__FILE__)."/"."../runs/$sRunName";
    return($sRunDir);
  }
}
