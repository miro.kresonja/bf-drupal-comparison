<?php

include_once(dirname(__FILE__)."/"."SitePuller.php");
include_once(dirname(__FILE__)."/"."Diff.php");

class ComparisonManager {
  /**
   * @var ClientSettingsManager $oClientSettings
   */
  private $oClientSettings;

  private $iPageLimit = -1;

  const DETAIL_NONE = 0;
  const DETAIL_PAGE = 1;
  const DETAIL_FULL = 2;
  private $eShowDetail = self::DETAIL_NONE;

  private $iPageCount = 0; // so we keep a tally of how many files we ran through

  function __construct($oClientSettings) {
    $this->oClientSettings = $oClientSettings;
  }

  function setPageLimit($iPageLimit) {
    $this->iPageLimit = $iPageLimit;
  }

  function setShowDetail($eShowDetail) {
    $this->eShowDetail = $eShowDetail;
  }

  function runComparison($sRun1, $sRun2) {
    $aResults = $this->getComparisonResults($sRun1, $sRun2);

    $iProblems = sizeof($aResults);
    print "Total pages processed: ".$this->iPageCount."\n";
    if ($iProblems > 0) {
      print "Encountered $iProblems issues\n";
      if ($this->eShowDetail == self::DETAIL_PAGE) {
        print "PAGES:\n";
        foreach ($aResults as $aResult) {
          print "\t".$aResult['file']."\n";
        }
      }
    } else {
      print "ALL GOOD!\n";
    }
  }

  function getComparisonResults($sRun1, $sRun2) {
    $aErrors = [];
    $aStartingFileList = $this->getRunFiles($sRun1);
    $aFileComparisonList = $this->getRunFiles($sRun2);

    $iTotalFiles = sizeof($aStartingFileList);
    $this->iPageCount = 0;
    foreach ($aStartingFileList as $sFile => $sFullPath) {
      $this->iPageCount++;
      if ($this->iPageLimit != -1 && $this->iPageCount > $this->iPageLimit) {
        break;
      }
      if ($this->iPageCount == 1 || $this->iPageCount % 20 == 0) {
        print "Pages: ".$this->iPageCount." / ".$iTotalFiles."\n";
      }
      $sFullPathStartFile = "";
      if (!array_key_exists($sFile, $aFileComparisonList)) {
        array_push($aErrors, [
          "status" => "error",
          "file" => $sFile,
          "message" => "File missing from run $sRun2: $sFile"
        ]);
        continue;
      }
      $sFullPathComparisonFile = $aFileComparisonList[$sFile];
      $aStatusHash = $this->compareTwoFiles($sFullPath, $sFullPathComparisonFile);

      if ($aStatusHash['status'] != "ok") {
        $aStatusHash["file"] = $sFile;
        array_push($aErrors, $aStatusHash);
      }
    }

    // now return results
    return($aErrors);
  }

  function getRunFiles($sRunName) {
    $aFileList = [];
    $sRunDir = SitePuller::getRunDir($sRunName);

    $d = dir($sRunDir);
    while (false !== ($sFileName = $d->read())) {
      if ($sFileName != ".." && $sFileName != ".") {
        $sFullFilePath = $sRunDir."/".$sFileName;
        $aFileList[$sFileName] = $sFullFilePath;
      }
    }

    return($aFileList);
  }

  function compareTwoFiles($sFile1Path, $sFile2Path) {
    $aStatusHash = [
      'status' => "ok"
    ];
    $aDiff = Diff::compareFiles($sFile1Path, $sFile2Path);
    $aRealDiff = [];
    foreach ($aDiff as $aDiffLine) {
      if ($aDiffLine[1] != Diff::UNMODIFIED) {
        array_push($aRealDiff, $aDiffLine);
      }
    }

    // this will remove the diff lines we explicitly asked to not be considered, based on our config file
    $aRemainingDiff = $this->findRemainingDiff($aRealDiff);

    $bHasDifferences = sizeof($aRemainingDiff) > 0;
    if ($bHasDifferences && $this->eShowDetail == self::DETAIL_FULL) {
      print "--- DIFF ---";
      print "FILE1: $sFile1Path\n";
      print "FILE2: $sFile2Path\n";
      print_r($aRemainingDiff);
      print "--- END DIFF ---\n";
    }

    if ($bHasDifferences) {
      $aStatusHash = [
        'status' => "diff",
        'message' => "Diff exists",
        'data' => $aRemainingDiff
      ];
    }
    return($aStatusHash);
  }

  function findRemainingDiff($aDifferences) {
    $aRemainingDiff = [];
    foreach ($aDifferences as $aDiffHash) {
      $sLine = $aDiffHash[0];
      $bIsRealDiff = $this->isThisARealDiffForOneLine($sLine);
      if ($bIsRealDiff) {
        array_push($aRemainingDiff, $aDiffHash);
      }
    }

    return($aRemainingDiff);
  }

  /**
   * If any of the config file exceptions are found, we return false, since this is an excluded diff
   * There are two subarrays in there, "strpos" and "preg_match", depending of what method you want to use
   * strpos should be faster, so use that first
   * @param $sLine
   * @return bool
   */
  function isThisARealDiffForOneLine($sLine) {
    $aExclusionDirs = $this->oClientSettings->aExclusionDirs;

    if (array_key_exists("strpos", $aExclusionDirs)) {
      foreach ($aExclusionDirs["strpos"] as $sFind) {
        if (strpos($sLine, $sFind) !== false) {
          return(false);
        }
      }
    }

    if (array_key_exists("preg_match", $aExclusionDirs)) {
      foreach ($aExclusionDirs["preg_match"] as $sRegExp) {
        if (preg_match($sRegExp, $sLine, $aMatches)) {
          return(false);
        }
      }
    }

    // can't find a reason to disqualify this difference, so it's a real difference
    return(true);
  }
}