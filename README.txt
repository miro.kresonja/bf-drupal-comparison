How to use:

0) Set settings file (with main url) clientsettings.php in this dir, copy the sample over

A) SET BASELINE:

1) php pullurls.php (do this while baseline is active) -> produces url.list
2) php pullsite.php --run="baseline"

B) DO THE FIRST CHANGE/UPGRADE, THEN:

3) php pullsite.php --run="stage1"
4) php runcomparison.php --run1="baseline" --run2="stage1" --pagelimit=1 --showdetail=full

... set exclusions in settings file from that one run
... if you want to omit files (for whatever reason), just edit the url.list file
... get to 0 problems in the run

C) FOR EACH SUBSEQUENT CHANGE:

4) php quickcompare.php

... this runs two things for you:
pullsite --run="YYYYMMDDHHMMSS"
php runcomparison.php --run1="baseline" --run2="YYYYMMDDHHMMSS"--showdetail=page

Each change should come through without html issues.

IF THAT IS TOO CUMBERSOME, MAYBE USE KOMPARE.
Install with:
sudo apt-get install kompare

Run on full directories with:
kompare ./run/baseline ./run/YYYYMMDDHHMMSS (or whatever)

