<?php

include_once(dirname(__FILE__)."/"."lib/ClientSettingsManager.php");

$aArgs = CommandRunner::getArguments();
$oClientManager = new ClientSettingsManager();

// quickcompare still listens to showdetail and pagelimit params
$comparisonManager = new ComparisonManager($oClientManager);
if (isset($aArgs["options"]["pagelimit"])) {
  $iPageLimit = intval($aArgs["options"]["pagelimit"]);
  if ($iPageLimit > 0) {
    $comparisonManager->setPageLimit($iPageLimit);
  }
}
if (isset($aArgs["options"]["showdetail"])) {
  $sShowDetail = $aArgs["options"]["showdetail"];
  if ($sShowDetail == "full") {
    $comparisonManager->setShowDetail(ComparisonManager::DETAIL_FULL);
  } else if ($sShowDetail == "page") {
    $comparisonManager->setShowDetail(ComparisonManager::DETAIL_PAGE);
  }
}

$sRunName1 = "baseline";
$sRunName2 = date("YmdHis");

print <<<EOH

====================================================
New Run Name: $sRunName2
====================================================

EOH;

sleep(2);

// do a new pull now
$puller = new SitePuller($oClientManager);
$puller->pullSite($sRunName2);

print <<<EON

====================================================
Running Comparison between $sRunName1 and $sRunName2
====================================================

EON;

$comparisonManager->runComparison($sRunName1, $sRunName2);
