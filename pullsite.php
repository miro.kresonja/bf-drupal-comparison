<?php

include_once(dirname(__FILE__)."/"."lib/ClientSettingsManager.php");

$oClientManager = new ClientSettingsManager();

// get a run name, if one is provided
$aArgs = $oClientManager->aArgs;
$sRunName = '';
if (isset($aArgs["options"]["run"])) {
  $sRunName = $aArgs["options"]["run"];
}

//print "RUN IS ".$sRunName."\n";
$puller = new SitePuller($oClientManager);
$puller->pullSite($sRunName);
