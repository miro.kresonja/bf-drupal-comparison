<?php

include_once(dirname(__FILE__)."/"."lib/ClientSettingsManager.php");

$aArgs = CommandRunner::getArguments();

$oClientManager = new ClientSettingsManager();

$getter = new TopUrlLevelGetter();
// next two lines are Drupal specific
$getter->addSkipRegex('/\/user\//');
$getter->addSkipRegex('/\/file\//');

$sURLListFile = $oClientManager->getSiteListFilePath();
$getter->saveURLListForTopXLevels($oClientManager->sSiteURL, 1, $sURLListFile);

$iPages = $getter->getProcessedCount();

print "Found $iPages pages\n";