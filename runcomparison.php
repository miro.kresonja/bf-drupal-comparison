<?php

include_once(dirname(__FILE__)."/"."lib/ClientSettingsManager.php");

$aArgs = CommandRunner::getArguments();
$oClientManager = new ClientSettingsManager();

$comparisonManager = new ComparisonManager($oClientManager);
if (isset($aArgs["options"]["pagelimit"])) {
  $iPageLimit = intval($aArgs["options"]["pagelimit"]);
  if ($iPageLimit > 0) {
    $comparisonManager->setPageLimit($iPageLimit);
  }
}

if (isset($aArgs["options"]["showdetail"])) {
  $sShowDetail = $aArgs["options"]["showdetail"];
  if ($sShowDetail == "full") {
    $comparisonManager->setShowDetail(ComparisonManager::DETAIL_FULL);
  } else if ($sShowDetail == "page") {
    $comparisonManager->setShowDetail(ComparisonManager::DETAIL_PAGE);
  }
}

if (!isset($aArgs["options"]["run1"]) || !isset($aArgs["options"]["run2"])) {
  print <<<USAGE
Usage: php runcomparison.php --run1="<FIRST RUN NAME>" --run2="<SECOND RUN NAME>" {--showdetail=[full|page]} {--pagelimit=[#]}

USAGE;
  die();
}
$sRunName1 = $aArgs["options"]["run1"];
$sRunName2 = $aArgs["options"]["run2"];

$comparisonManager->runComparison($sRunName1, $sRunName2);
